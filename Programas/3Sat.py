"""Mendoza Castillo María Fernanda.
   Complejidad Computacional.
   22.Febrero.2020. Programa 01."""

from random import randint 

"""makeInstance. Build an array of random 10 variables."""
def makeInstance(variables):
	clause = []
	clause_val = []
	for i in range(0, 15):
		var = randint(0, 9)
		clause_val.append(variables[var])
		if (variables[var] == 0):
			clause.append("~" + vars_names[var]) # "~" if (not v).
		else:
			clause.append(vars_names[var])
	return clause  #List of variables.


"""checkSat. Change random values for SAT (Fase adivinadora)."""
def checkSat(instance):
	for i in range(len(instance)): 
		coin = randint(0, 1) #Random as throwing a coin.
		if (coin == 0):
			instance[i] = 0  # "Sol"
		else:
			instance[i] = 1  # "Águila"
	return instance


"""cSat. Value of each clause."""
def cSat(vars_list):
	c1 = vars_list[0] or vars_list[1] or vars_list[2]
	c2 = vars_list[3] or vars_list[4] or vars_list[5]
	c3 = vars_list[6] or vars_list[7] or vars_list[8]
	c4 = vars_list[9] or vars_list[10] or vars_list[11]
	c5 = vars_list[12] or vars_list[13] or vars_list[14]
	c_list = [c1, c2, c3, c4, c5]
	return c_list

"""verify. Is the instance SAT? (Fase verificadora)."""
def verify(sat_inst):
	for i in range(len(sat_inst)):
		if sat_inst[i] == 0:
			return "NO"
	return "SÍ"



#Random variables (0, 1) (Our instance).
v0 = randint(0, 1)
v1 = randint(0, 1)
v2 = randint(0, 1)
v3 = randint(0, 1)
v4 = randint(0, 1)
v5 = randint(0, 1)
v6 = randint(0, 1)
v7 = randint(0, 1)
v8 = randint(0, 1)
v9 = randint(0, 1)
vars_names = ["v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9"]
variables = [v0, v1, v2, v3, v4, v5, v6, v7, v8, v9]

insta = makeInstance(variables)
print("Ejemplar aleatorio:")
print(insta)
checked = checkSat(insta)
print ("Asignación de la fase adivinadora:")
print(checked)	
verified = verify(cSat(checked))
print("¿Es satisfacible?")
print(verified)