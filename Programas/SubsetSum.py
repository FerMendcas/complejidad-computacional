def mergeLists(l1, l2):
	return list(set(sorted(l1 + l2)))

def exactSubsetSum(S, t):
	n = len(S)
	L = [0]
	for i in range(n):
		suma = [n + S[i] for n in L] 
		L = mergeLists(L, suma)
		for x in L:
			if(x > t):
				L.remove(x)
	print(max(L))

def trim(L, d):
	m = len(L)
	Lf = [L[0]]
	last = L[0]
	for i in range(1, m):
		if(L[i] > last * (1 + d)):
			Lf.append(L[i])
			last = L[i]
	return Lf

def approxSubsetSum(S, t, e):
	n = len(S)
	L = [0]
	d = e / (2 * n)
	for i in range(n):
		suma = [n + S[i] for n in L] 
		L = mergeLists(L, suma)
		L = trim(L, d)
		for x in L:
			if(x > t):
				L.remove(x)
	print(max(L))	


print("Ingrese los valores del conjunto S separados por un espacio sin corchetes(Ej. x y z): ")
S = list(map(int, input().split()))
print("Ingrese el valor de t:")
t = int(input())
print("Ingrese el valor de epsilon (0 <= e <= 1):")
e = float(input())

print("Valores del ejemplar: (S, t, e)")
print(S)
print(t)
print(e)
print("Valor máximo de la suma de los conjuntos:")
exactSum = exactSubsetSum(S, t)
print("Valor máximo del algoritmo de aproximación:")
AppSum = approxSubsetSum(S, t, e)