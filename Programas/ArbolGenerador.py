from random import randrange, choice

"""Generación de la gráfica aleatoria."""
print("Elige el número de vértices para el ejemplar: ")
num = int(input()) # El usuario decide el número de vértices.
vertices = []
for i in range(num):
	vertices.append("v" + str(i))
print("Se generaron los siguientes vértices:")
print(vertices)

grafica = {}
aristas = randrange((((num - 1)*(num - 2)) // 2) + 1, (num * (num - 1)) // 2)
#Por teoría de gráficas para el rango de aristas para que sea conexa.
cont = 0
while(cont < aristas):
	va = randrange(0, num - 1)
	vb = randrange(1, num)
	items = grafica.get(vertices[va])
	if(va >= vb or (items != None and vertices[vb] in items)):
		continue
	else:
		grafica.setdefault(vertices[va], []).append(vertices[vb])
		cont += 1
print("Gráfica generada aleatoriamente:")
print(grafica)


"""Fase adivinadora. Tomamos una subgráfica aleatoria como propuesta a solución.
                     Se generaran solo de n-1 aristas, para que pueda ser árbol."""
while(aristas > num - 1): # Se eliminan aristas hasta el mínimo requerido para un árbol.
	ua = choice(list(grafica.keys()))
	item = grafica.get(ua)
	index = randrange(0, len(item))
	if(item != None and len(item) > 1 and item[index] in item):
		item.remove(item[index])
		aristas -= 1
	else:
		grafica.pop(ua)
		aristas -= 1
print("La propuesta de solución:")
print(grafica)


"""Fase verificadora. Revisamos que estén todos los vértices y no tenga ciclos."""
lista = []
for elem in grafica.keys():
	for item in grafica.get(elem):
		lista.append(item)

solucion = False
for i in vertices:
	if(i not in grafica.keys() and i not in lista):
		print("NO") # No están todos los vértices de la gráfica.
	else:
		for j in grafica.keys():
			for k in grafica.get(j):
				for l in grafica.get(j):
					if(grafica.get(k) != None and l in grafica.get(k)):
						print("NO") # Tiene ciclos.
						if(not solucion):
							break
					else:
						continue
	print("YES")
	if(solucion):
		break
	else:
		break